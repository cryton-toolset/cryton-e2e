# PROJECT HAS BEEN MOVED
The project has been moved to https://gitlab.ics.muni.cz/cryton/cryton. For more information check the [documentation](https://cryton.gitlab-pages.ics.muni.cz/).

# Cryton E2E

## Description
Cryton E2E is a project used for End-To-End testing of the Cryton toolset.

Cryton toolset is tested and targeted primarily on **Debian** and **Kali Linux**. Please keep in mind that **only 
the latest version is supported** and issues regarding different OS or distributions may **not** be resolved.

For more information see the [documentation](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/development/e2e-testing/).

## Quick-start
The easiest way to run the E2E tests is to utilize the Cryton playground, please follow the instructions in the [documentation](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/playground/introduction/).

## Contributing
Contributions are welcome. Please **contribute to the [project mirror](https://gitlab.com/cryton-toolset/cryton-e2e)** on gitlab.com.
For more information see the [contribution page](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/contribution-guide/).
