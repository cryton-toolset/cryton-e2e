from typing import List, Dict, Optional

from cryton_e2e.lib.tests.test_run import TestRun


class TestRunExecution(TestRun):
    """
    Base class for Run execution testing.
    """
    def __init__(self, template: str, workers: List[Dict], inventories: Optional[List[str]],
                 execution_variables: Optional[List[str]], max_timeout: int):
        super().__init__(template, workers, inventories, execution_variables, max_timeout)
        self.description = "Setup and execute a Run."

    def _run_tests(self):
        """
        Runs the tests.
        :return: None
        """
        check_word = "successfully"

        # Execute Run
        self.test("Executing Run", ["runs", "execute", str(self.run_id)], check_word, "FINISHED")

        # Get Run report
        self.test("Getting Run's report", ["runs", "report", str(self.run_id), "--localize"], check_word)

        # Check if any steps failed
        self.get_step_results()
