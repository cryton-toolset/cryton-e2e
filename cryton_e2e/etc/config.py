from os import getenv, path
from pathlib import Path
from dotenv import load_dotenv


APP_DIRECTORY = getenv("CRYTON_E2E_APP_DIRECTORY", Path(__file__).parent.parent.parent.absolute())

load_dotenv(path.join(APP_DIRECTORY, ".env"))

RESOURCES_DIRECTORY = path.join(APP_DIRECTORY, "resources")
TEMPLATES_DIRECTORY = path.join(RESOURCES_DIRECTORY, "templates")
INVENTORY_DIRECTORY = path.join(RESOURCES_DIRECTORY, "inventories")

TESTS = getenv("CRYTON_E2E_TESTS", "all")

CRYTON_CLI_EXECUTABLE = getenv("CRYTON_E2E_CRYTON_CLI_EXECUTABLE", "cryton-cli").split(" ")

WORKER_ADDRESS = getenv("CRYTON_E2E_WORKER_ADDRESS", "127.0.0.1")
WORKER_NAME = getenv("CRYTON_E2E_WORKER_NAME", "Worker")
